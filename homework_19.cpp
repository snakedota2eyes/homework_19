﻿#include <iostream>

using namespace std;
class	Animal
{
public:
	virtual void Voice()
	{
		cout << "It's animal" << endl;
	}

	virtual ~Animal() {}

};
class Dog : public Animal
{
public:
	void Voice() override
	{
		cout << "Woof" << endl;
	}
};
class Cat : public Animal
{
public:
	void Voice() override
	{
		cout << "Meow meow" << endl;
	}
};
class Fox : public Animal
{
public:
	void Voice() override
	{
		cout << "Ding ding ring ding" << endl;
	}
};


int main()
{
	Animal* array[3];
	array[0] = new Dog;
	array[1] = new Cat;
	array[2] = new Fox;

	for (int i = 0; i < 3; i++)
	{
		array[i]->Voice();
	}

	for (int i = 0; i < 3; i++)
	{
		delete array[i];
	}
}
